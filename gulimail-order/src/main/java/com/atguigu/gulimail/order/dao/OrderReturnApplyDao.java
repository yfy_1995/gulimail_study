package com.atguigu.gulimail.order.dao;

import com.atguigu.gulimail.order.entity.OrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author yfy
 * @email yfy@gmail.com
 * @date 2020-12-12 15:21:24
 */
@Mapper
public interface OrderReturnApplyDao extends BaseMapper<OrderReturnApplyEntity> {
	
}
