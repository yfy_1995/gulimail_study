package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author yfy
 * @email yfy@gmail.com
 * @date 2020-12-12 15:09:31
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {
	
}
