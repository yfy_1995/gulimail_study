package com.atguigu.gulimail.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 远程调用别的服务的步骤
 *  1.引入openfeign
 *  2.编写一个接口，告诉Spring Cloud这个接口需要调用远程服务
 *      1)声明接口的每一个方法都是调用哪个远程服务的哪个请求
 *      2）开启远程调用功能  @EnableFeignClients
 */
@EnableFeignClients(basePackages = "com.atguigu.gulimail.member.feign")
@SpringBootApplication
@EnableDiscoveryClient
public class GulimailMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimailMemberApplication.class, args);
    }

}
