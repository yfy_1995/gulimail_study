package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.MemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author yfy
 * @email yfy@gmail.com
 * @date 2020-12-12 15:09:31
 */
@Mapper
public interface MemberCollectSubjectDao extends BaseMapper<MemberCollectSubjectEntity> {
	
}
