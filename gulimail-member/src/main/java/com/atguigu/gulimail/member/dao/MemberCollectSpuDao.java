package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author yfy
 * @email yfy@gmail.com
 * @date 2020-12-12 15:09:31
 */
@Mapper
public interface MemberCollectSpuDao extends BaseMapper<MemberCollectSpuEntity> {
	
}
