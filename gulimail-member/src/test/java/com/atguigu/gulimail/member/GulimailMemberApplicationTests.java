package com.atguigu.gulimail.member;

import com.atguigu.gulimail.member.entity.MemberEntity;
import com.atguigu.gulimail.member.service.MemberService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GulimailMemberApplicationTests {
    @Autowired
    private MemberService memberService;

    @Test
    void contextLoads() {
        for (MemberEntity memberEntity : memberService.list()) {
            System.out.println(memberEntity);
        }
    }

}
