package com.atguigu.gulimail.thirdparty;

import com.aliyun.oss.OSSClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimailThirdPartyApplicationTests {
    @Autowired
    OSSClient ossClient;

    @Test
    public void saveFile() throws FileNotFoundException {
        // download file to local
        ossClient.putObject("yfy-test", "test2.jpg", new FileInputStream("C:\\Users\\yfy\\Desktop\\图片素材\\壁纸\\0j1rdw.jpg"));
        ossClient.shutdown();

        System.out.println("上传完成");
    }
}
