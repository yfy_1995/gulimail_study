package com.atguigu.gulimail.product;

import com.atguigu.gulimail.product.entity.BrandEntity;
import com.atguigu.gulimail.product.service.BrandService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.redisson.client.RedisClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimailProductApplicationTests {
    @Autowired
    private BrandService brandService;
    //    @Autowired
//    OSSClient ossClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedissonClient redissonClient;

    @Test
    public void redisson(){
        System.out.println(redissonClient);
    }

    @Test
    public void testRedis(){
        stringRedisTemplate.opsForValue().set("test","001");
    }

    @Test
    public void getRedis(){
        System.out.println(stringRedisTemplate.opsForValue().get("test"));
    }


    @Test
    public void testBrandService() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setDescript("第二大的电商网站");
        brandEntity.setName("京东");
        brandService.save(brandEntity);
        System.out.println("保存成功");
    }

//    @Test
//    public void testUpload() throws FileNotFoundException {
//        // Endpoint以杭州为例，其它Region请按实际情况填写。
//        String endpoint = "oss-cn-beijing.aliyuncs.com";
//        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
//        String accessKeyId = "LTAI4GF2e8dSph99Xkfzq2Ln";
//        String accessKeySecret = "8YPE6pvXmUxKtnDtkgLyyynEW2XHfN";
//
//        // 创建OSSClient实例。
//        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
//
//        // 上传文件流。
//        InputStream inputStream = new FileInputStream("C:\\Users\\yfy\\Desktop\\图片素材\\壁纸\\0j1rdw.jpg");
//        ossClient.putObject("yfy-test", "test.jpg", inputStream);
//
//        // 关闭OSSClient。
//        ossClient.shutdown();
//
//        System.out.println("上传完成");
//    }

//    @Test
//    public void saveFile() throws FileNotFoundException {
//        // download file to local
//        ossClient.putObject("yfy-test", "test1.jpg", new FileInputStream("C:\\Users\\yfy\\Desktop\\图片素材\\壁纸\\0j1rdw.jpg"));
//        ossClient.shutdown();
//
//        System.out.println("上传完成");
}

