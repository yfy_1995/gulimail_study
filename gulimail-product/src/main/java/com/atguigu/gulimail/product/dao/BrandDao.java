package com.atguigu.gulimail.product.dao;

import com.atguigu.gulimail.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author yfy
 * @email yfy@gmail.com
 * @date 2020-12-12 14:57:22
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
