package com.atguigu.gulimail.product.dao;

import com.atguigu.gulimail.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author yfy
 * @email yfy@gmail.com
 * @date 2020-12-15 16:30:03
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
