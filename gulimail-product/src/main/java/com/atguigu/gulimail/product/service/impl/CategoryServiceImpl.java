package com.atguigu.gulimail.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.gulimail.product.service.CategoryBrandRelationService;
import com.atguigu.gulimail.product.vo.Catelog2Vo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimail.product.dao.CategoryDao;
import com.atguigu.gulimail.product.entity.CategoryEntity;
import com.atguigu.gulimail.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {
    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private RedissonClient redissonClient;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        // 1.查出所有分类
        List<CategoryEntity> list = baseMapper.selectList(null);

        // 2.组装成父子的树形结构

        // 2.1 找到所有的一级分类
        List<CategoryEntity> level1Menu = list.stream().filter((categoryEntity) -> categoryEntity.getCatLevel() == 1).map(menu -> {
            // 设置二级分类
            menu.setChildren(getChildrens(menu, list));
            return menu;
        }).sorted(Comparator.comparingInt(item -> (item.getSort() == null ? 0 : item.getSort()))).collect(Collectors.toList());

        return level1Menu;
    }

    @Override
    public void removeMenuByIds(List<Long> asList) {
        // TODO: 2020/12/15 检查当前要删除的菜单是否被别的地方引用
        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] findCatelogPath(Long catelogId) {
        LinkedList<Long> paths = new LinkedList<>();
        findParentPath(catelogId, paths);
        return paths.toArray(new Long[0]);
    }

    @Transactional
    @Override
    public void updateDetail(CategoryEntity category) {
        this.updateById(category);
        if (!StringUtils.isEmpty(category.getName())) {
            categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
        }
        //同时修改或删除缓存中的数据
    }

    @Override
    public List<CategoryEntity> getLevel1Categorys() {
        return this.baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
    }

    // TODO: 2020/12/26 堆外内存溢出
    // 1)springboot2.0之后默认使用lettuce做操作redis客户端。使用netty进行网络通信。
    // 2）lettuce的bug导致netty堆外内存溢出。我们启动配置了-Xmx300M netty如果没有指定堆外内存，默认使用-Xmx300M
    // 3)可以通过-Dio.netty.maxDirectMemory进行设置
    //解决方法：
    // 1)升级lettuce客户端
    // 2)切换使用jedis
    @Override
    public Map<String, List<Catelog2Vo>> getCatelogJson() {
        /**
         * 空结果缓存：解决缓存穿透
         * 设置随机过期时间：解决缓存雪崩
         * 数据库查询加锁：解决缓存击穿
         */

        String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
        System.out.println("catalogJSON" + catalogJSON);
        if (StringUtils.isEmpty(catalogJSON)) {
            Map<String, List<Catelog2Vo>> catelogJsonFromDb = getCatalogJsonFromDbWithRedissonLock();
            System.out.println("catalogJSON fromdb" + catelogJsonFromDb);
            redisTemplate.opsForValue().set("catalogJSON", JSON.toJSONString(catelogJsonFromDb), 1, TimeUnit.DAYS);

            return catelogJsonFromDb;
        }

        //转为指定的对象
        Map<String, List<Catelog2Vo>> results = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
        });
        return results;
    }

    /**
     * 缓存里面的数据如何和数据保持一致
     *  1）双写模式
     *  2）失效模式
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedissonLock() {
        //锁的名称。锁的粒度越小越好
        //锁的粒度：具体缓存的是哪个数据，比如11号商品：product-11-lock
        RLock lock = redissonClient.getLock("catalogJson-lock");
        lock.lock();
        Map<String, List<Catelog2Vo>> dataFromDb;
        try {
            dataFromDb = getDataFromDb();
        } finally {
            lock.unlock();
        }
        return dataFromDb;
    }


    /**
     * 从数据库查询并封装分类数据
     *
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDbWithRedisLock() {
        /**
         * 将数据库的多次查询变成一次
         */
        String uuid = UUID.randomUUID().toString();
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("lock", uuid, 300, TimeUnit.SECONDS);
        if (lock) {
            //加锁成功
            //设置过期时间，必须和加锁是同步的，原子的
//            redisTemplate.expire("lock", 30, TimeUnit.SECONDS);
            try {
                Map<String, List<Catelog2Vo>> dataFromDb = getDataFromDb();
                return dataFromDb;
            } finally {
                String script = "if redis.call(\"get\",KEYS[1]) == ARGV[1]\n" +
                        "then\n" +
                        "    return redis.call(\"del\",KEYS[1])\n" +
                        "else\n" +
                        "    return 0\n" +
                        "end";
                //获取值对比+删除=原子操作
//            String lockValue = redisTemplate.opsForValue().get("lock");
//            if (uuid.equals(lockValue)) {
//                redisTemplate.delete("lock");
//            }
                // http://redis.cn/commands/set.html
                //删除锁
                redisTemplate.execute(new DefaultRedisScript<>(script, Long.class), Arrays.asList("lock"), uuid);
            }

        } else {
            //加锁失败,重试
            //休眠100ms
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
            return getCatalogJsonFromDbWithRedisLock();//自旋的方式
        }
    }

    private Map<String, List<Catelog2Vo>> getDataFromDb() {
        List<CategoryEntity> selectList = baseMapper.selectList(null);

        //1. 查出所有1级分类
        List<CategoryEntity> level1Categorys = getParentCid(selectList, 0L);
        //2.封装数据
        Map<String, List<Catelog2Vo>> collect = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //查询这个一级分类下的所有二级分类
            List<CategoryEntity> categoryEntities = getParentCid(selectList, v.getCatId());
            List<Catelog2Vo> catelog2VoList = null;
            if (categoryEntities != null) {
                catelog2VoList = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    //找当前二级分类的三级分类
                    List<CategoryEntity> level3CategoryEntityList = getParentCid(selectList, l2.getCatId());
                    if (level3CategoryEntityList != null) {
                        List<Catelog2Vo.Catalog3Vo> catelog3VoList = level3CategoryEntityList.stream().map(l3 -> new Catelog2Vo.Catalog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName())).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(catelog3VoList);
                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2VoList;
        }));

        return collect;
    }

    private List<CategoryEntity> getParentCid(List<CategoryEntity> selectList, Long parentCid) {
        return selectList.stream().filter(item -> item.getParentCid().equals(parentCid)).collect(Collectors.toList());
//        return this.baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", parentCid));
    }

    public void findParentPath(Long catelogId, LinkedList<Long> paths) {
        CategoryEntity categoryEntity = this.getById(catelogId);
        if (categoryEntity != null) {
            paths.addFirst(categoryEntity.getCatId());
            if (categoryEntity.getParentCid() != 0) {
                findParentPath(categoryEntity.getParentCid(), paths);
            }
        }
    }

    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> list) {
        return list.stream().filter((categoryEntity) -> categoryEntity.getParentCid().equals(root.getCatId())).map((categoryEntity -> {
            //递归调用获取自己的子分类
            categoryEntity.setChildren(getChildrens(categoryEntity, list));
            return categoryEntity;
        })).sorted(Comparator.comparingInt(item -> (item.getSort() == null ? 0 : item.getSort()))).collect(Collectors.toList());
    }

}