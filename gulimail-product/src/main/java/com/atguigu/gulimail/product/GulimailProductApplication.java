package com.atguigu.gulimail.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 整合mybatis plus
 *  1）导入依赖 common已导入
 *  2） 配置： https://baomidou.com/guide/quick-start.html#%E9%85%8D%E7%BD%AE
 *       1.配置数据源
 *          导入数据库驱动
 *          application.yml配置数据源相关信息
 *       2.配置mybatis-plus
 *          在 Spring Boot 启动类中添加 @MapperScan 注解，扫描 Mapper 文件夹
 *          告诉mybatis-plus,sql映射文件位置
 *          配置主键自增
 *
 *
 *  JSR303分组校验（多场景的复杂校验）
 *   @Null(message = "新增不能指定品牌id", groups = {AddGroup.class}) 给注解标注什么情况需要进行校验 AddGroup为自定义的空接口
 *   controller指定校验分组 @Validated(value = AddGroup.class)
 *   默认没有指定分组的校验注解，在分组校验下@Validated(value = AddGroup.class)不会生效，只会在@Valid生效
 *
 *
 *   自定义校验
 *      1）编写一个自定义的校验注解
 *      2）编写一个自定义的校验器
 *      3）关联自定义的校验器和自定义的校验注解
 *
 *
 *
 *  模板引擎
 *      1）thymeleaf-starter：关闭缓存
 *      2）静态资源都放在static文件夹下就可以按照路径直接访问
 *      3）页面放在templates下，直接访问
 *       SpringBoot访问项目的时候，默认会找index.html
 */
@MapperScan("com.atguigu.gulimail.product.dao")
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.atguigu.gulimail.product.feign")
public class GulimailProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimailProductApplication.class, args);
    }

}
