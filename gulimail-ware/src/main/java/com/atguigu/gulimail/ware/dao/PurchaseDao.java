package com.atguigu.gulimail.ware.dao;

import com.atguigu.gulimail.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author yfy
 * @email yfy@gmail.com
 * @date 2020-12-12 15:24:01
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
