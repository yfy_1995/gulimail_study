package com.atguigu.gulimail.ware.dao;

import com.atguigu.gulimail.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author yfy
 * @email yfy@gmail.com
 * @date 2020-12-12 15:24:01
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
