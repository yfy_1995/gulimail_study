package com.atguigu.gulimail.coupon;

import com.atguigu.gulimail.coupon.entity.CouponEntity;
import com.atguigu.gulimail.coupon.service.CouponService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GulimailCouponApplicationTests {
    @Autowired
    private CouponService couponService;

    @Test
    void contextLoads() {
        List<CouponEntity> list = couponService.list();
        for (CouponEntity couponEntity : list) {
            System.out.println(couponEntity);
        }
    }

}
