package com.atguigu.gulimail.coupon.dao;

import com.atguigu.gulimail.coupon.entity.SkuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author yfy
 * @email yfy@gmail.com
 * @date 2020-12-12 15:03:50
 */
@Mapper
public interface SkuFullReductionDao extends BaseMapper<SkuFullReductionEntity> {
	
}
