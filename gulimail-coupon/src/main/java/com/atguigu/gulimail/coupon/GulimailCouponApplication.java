package com.atguigu.gulimail.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 如何使用Nacos作为配置中心
 *  1.引入依赖
 *  2.创建一个bootstrap.properties
 *  3.引入注解@RefreshScope可以动态获取配置  @Valid可以获取某个配置的值
 *  4.给配置中心默认添加一个`当前应用名.properties`的配置文件
 *
 *  官方提供的案例： https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-examples/nacos-example/nacos-config-example/readme.md
 *
 *  细节：
 *      1. 命名空间：配置隔离
 *          默认public(保留空间)：默认新增的所有配置都在Public空间
 *          1）开发、测试、生产：利用命名空间来做环境隔离
 *              spring.cloud.nacos.config.namespace={dataId}
 *          2）每一个微服务之间互相隔离，每一个微服务都创建自己的命名空间，只加载自己命名空间下的所有配置
 *      2. 配置集
 *          一组相关或者不相关的配置项的集合成为配置集
 *      3. 配置集id：类似文件名-DataId
 *      4. 配置集分组
 *          默认所有的配置集都属于 DEFAULT_GROUP
 *
 *   每个微服务创建自己的命名空间，使用配置分组来区分环境 dev/test/prod
 *
 *   加载多个配置集：
 *      1）微服务任何配置文件都可以放在配置中心中
 *      2）只需要在bootstrap.properties中来说明加载配置中心哪些配置文件即可
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
public class GulimailCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimailCouponApplication.class, args);
    }

}
