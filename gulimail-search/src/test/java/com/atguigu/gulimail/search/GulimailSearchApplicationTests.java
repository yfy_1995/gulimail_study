package com.atguigu.gulimail.search;

import com.alibaba.fastjson.JSON;
import com.atguigu.gulimail.search.config.ElasticSearchConfig;
import lombok.Data;
import lombok.ToString;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.Map;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimailSearchApplicationTests {
    @Autowired
    private RestHighLevelClient client;

    @Test
    public void contextLoads() {
        System.out.println(client);
    }

    @Data
    class User {
        private String userName;
        private String gender;
        private Integer age;

        public User(String userName, String gender, Integer age) {
            this.userName = userName;
            this.gender = gender;
            this.age = age;
        }
    }

    /**
     * 测试存储数据到es
     */
    @Test
    public void indexData() throws IOException {
        IndexRequest request = new IndexRequest("users");
        request.id("1");
//        request.source("userName","zhangsan","age",18,"gender","男");
        User user = new User("张三", "男", 18);
        String jsonString = JSON.toJSONString(user);
        request.source(jsonString, XContentType.JSON);
        //执行保存操作
        IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
        System.out.println(indexResponse);
    }

    @Data
    @ToString
    static class Account {
        private int account_number;
        private int balance;
        private String firstname;
        private String lastname;
        private int age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;
    }

    @Test
    public void searchData() throws IOException {
        //1.创建检索请求
        SearchRequest searchRequest = new SearchRequest();
        //指定索引
        searchRequest.indices("bank");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 搜索address中包含mill的所有人的年龄分布以及平均年龄
//        GET /bank/_search
//        {
//            "query": {
//            "match": {
//                "address": "mill"
//            }
//        },
//            "aggs": {
//            "ageAgg": {
//                "terms": { "field": "age" }
//            },
//            "avgAge": { "avg": { "field": "age" } }
//        },
//            "size":0
//        }
        sourceBuilder.query(QueryBuilders.termQuery("address", "mill"));
        sourceBuilder.from(0);
        sourceBuilder.size(20);
        TermsAggregationBuilder aggAggregation = AggregationBuilders.terms("ageAgg")
                .field("age");
        AvgAggregationBuilder avgAggregation = AggregationBuilders.avg("avgAge").field("age");
        sourceBuilder.aggregation(aggAggregation);
        sourceBuilder.aggregation(avgAggregation);
        System.out.println("sourceBuilder:" + sourceBuilder.toString());
        searchRequest.source(sourceBuilder);

        //2.执行检索请求
        SearchResponse search = client.search(searchRequest, ElasticSearchConfig.COMMON_OPTIONS);
        System.out.println(search.toString());
        //3.分析结果
        //3.1 获取所有查到的数据
        SearchHits hits = search.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit hit : searchHits) {
            String sourceAsString = hit.getSourceAsString();
            Account account = JSON.parseObject(sourceAsString, Account.class);
            System.out.println("account:" + account);
        }
        //3.2 获取聚合出来的分析数据
        Aggregations aggregations = search.getAggregations();
        Terms ageAgg = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : ageAgg.getBuckets()) {
            System.out.println("年龄:" + bucket.getKey()+"==="+bucket.getDocCount());
        }
        Avg avgAge = aggregations.get("avgAge");
        System.out.println("平均值:" + avgAge.getValue());
    }
}
